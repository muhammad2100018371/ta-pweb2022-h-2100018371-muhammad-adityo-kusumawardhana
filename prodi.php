<?php
    $array1=array("berserk","Naruto","One Piece");
    echo $array1[0]. "<br>";
    echo $array1[2]. "<br><br>";

    echo "Foreach <BR>";
    foreach($array1 as $anime){
        echo "Anime : ";
        echo $anime. "<br>";
    }

    echo "for <br>";
    for($i=0;$i<count($array1);$i++){
        echo "Anime : ";
        echo $anime. "<br>";
    }
    echo "<br>";

    $array2=array();
    $array2[]='Spongebob';
    $array2[]='Kiko';
    $array2[]='Chalkzone';
    echo $array2[0]. "<br>";
    echo $array2[2]. "<br><br>";

    $array3=array("A"=>35, "B"=>40);
    echo "Murid di Kelas A : ";
    echo $array3['A']. "<br>";
    echo "Murid di Kelas B : ";
    echo $array3['B']. "<br>";

    $array4=array();
    $array4['C']='36';
    $array4['D']='39';
    echo "Murid di Kelas C : ";
    echo $array4['C']. "<br>";
    echo "Murid di Kelas D : ";
    echo $array4['D']. "<br>";

    echo"<pre>";
    print_r($array1);
    echo "<br>";
    print_r($array3);
    echo "</pre>";

    sort($array3);
    reset($array3);
    echo "Urutkan Array3 dengan sort <br>";
    echo"<pre>";
    print_r($array3);
    echo "</pre>";

    rsort($array3);
    reset($array3);
    echo "Urutkan Array3 dengan rsort <br>";
    echo"<pre>";
    print_r($array3);
    echo "</pre>";

    asort($array3);
    reset($array3);
    echo "Urutkan Array3 dengan asort <br>";
    echo"<pre>";
    print_r($array3);
    echo "</pre>";

    arsort($array3);
    reset($array3);
    echo "Urutkan Array3 dengan arsort <br>";
    echo"<pre>";
    print_r($array3);
    echo "</pre>";

    ksort($array3);
    reset($array3);
    echo "Urutkan Array3 dengan ksort <br>";
    echo"<pre>";
    print_r($array3);
    echo "</pre>";

    krsort($array3);
    reset($array3);
    echo "Urutkan Array3 dengan krsort <br>";
    echo"<pre>";
    print_r($array3);
    echo "</pre>";

    echo"Mengatur Posisi Array ";
    echo "<pre>";
    print_r($array1);
    echo"</pre>";
    $mode=current($array1);
    echo $mode. "<br>";
    $mode=next($array1);
    echo $mode. "<br>";
    $mode=current($array1);
    echo $mode. "<br>";
    $mode=prev($array1);
    echo $mode. "<br>";
    $mode=end($array1);
    echo $mode. "<br>";
    $mode=current($array1);
    echo $mode. "<br>";

    echo "Mencari Elemen array <br>";
    if(in_array("Berserk",$array1)){
        echo "Ada Berserk di dalam array <br>";
    }
    else{
        echo "Tidak ada Berserk dalam array <br>";
    }

    echo "Fungsi : <br>";
    function cetak_segitiga(){
    $baris;
    $tinggi = 5;
    $i = 0;
    $j = 0;
        for($baris = 1;$baris<=$tinggi;$baris++){
            for($i=1;$i<=$tinggi-$baris;$i++){ 
            echo" ";
        }
        for($j=1;$j<2*$baris;$j++){
            echo"*";
        }
        echo"<br>";
        }
    }

    cetak_segitiga();

    echo"Dengan Parameter <br>";
    function cetak_segitiga1($tinggi){
    $baris;
    $i = 0;
    $j = 0;
        for($baris = 1;$baris<=$tinggi;$baris++){
            for($i=1;$i<=$tinggi-$baris;$i++){ 
            echo" ";
        }
        for($j=1;$j<2*$baris;$j++){
            echo"*";
        }
        echo"<br>";
        }
    }

    $a=5;
    cetak_segitiga1($a);

    function luas_persegi($sisi){
        return $sisi*$sisi;    
    }

    $a=2;
    echo "Luas Persegi : ";
    echo luas_persegi($a);
    echo "<br>";

    function tambah_string($str){
        $str=$str. ", Steam";
        return $str;
    }

    $string = "Summersale";
    echo tambah_string($string). "<br>";

    function tambah_string1(&$str){
        $str=$str. ", Steam";
        return $str;
    }

    $string = "Summersale";
    echo tambah_string($string). "<br>";

    $arr=get_defined_functions();
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
?>